.. MathHarbor Docs documentation master file, created by
   sphinx-quickstart on Mon Jan 20 11:02:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the MathHarbor Platform documentation!
===========================================

MathHarbor is a platform focused on cloud-based computational mathematics. Flexible, efficient, and collaborative numerical modeling, on the cloud. 

We are pushing towards releasing functional docs within a few days. Do check in in a few!

Contents:

.. toctree::
   :maxdepth: 2



